import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS

app = Flask(__name__)
app.config["DEBUG"] = True
app.config["SECRET_KEY"] = os.urandom(32)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///confab.sqlite"
app.config["WTF_CSRF_ENABLED"] = False

db = SQLAlchemy(app)
marsh = Marshmallow(app)
cors = CORS(app, resources=(r'/api/*', {'origins': '*'}))

from .helpers import *
from .models import *
from .forms import *
from .views.api.v1 import *
