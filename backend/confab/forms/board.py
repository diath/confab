from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField
from wtforms.validators import InputRequired

class BoardForm(FlaskForm):
    uri = StringField('uri', validators=[InputRequired()])
    name = StringField('name', validators=[InputRequired()])
    description = StringField('description', default='')
    nsfw = BooleanField(default=False)
