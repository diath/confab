from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import InputRequired, Length

class PostForm(FlaskForm):
    thread_id = IntegerField('thread_id', validators=[InputRequired()])
    content = StringField('content', validators=[InputRequired(), Length(min=2, max=2048)])
