from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, BooleanField
from wtforms.validators import URL, InputRequired, Optional, Length, ValidationError

def EitherValidator(other):
    def _validator(form, field):
        if not field.data and not getattr(form, other).data:
            raise ValidationError('You must specify either {} or {}.'.format(
                field.name, other
            ))

    return _validator

class ThreadForm(FlaskForm):
    board_id = IntegerField('board_id', validators=[InputRequired()])
    title = StringField('title', validators=[InputRequired(), Length(max=128)])
    link = StringField('link', validators=[URL(), Optional(), EitherValidator('content')])
    content = StringField('content', validators=[EitherValidator('link')])
    nsfw = BooleanField('nsfw', default=False)
