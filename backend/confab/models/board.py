from time import time

from confab import db, marsh
from .thread import Thread
from .post import Post

class Board(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    uri = db.Column(db.String(16), unique=True)
    name = db.Column(db.String(64))
    description = db.Column(db.Text)
    type = db.Column(db.Integer, default=0)
    nsfw = db.Column(db.Boolean, default=False)
    thread_limit = db.Column(db.Integer, default=50)
    bump_limit = db.Column(db.Integer, default=300)
    last_thread = db.Column(db.Integer, default=0)
    last_post = db.Column(db.Integer, default=0)

    def thread_count(self):
        result = db.session().query(db.func.count(Thread.id).label('count')).filter(Thread.board_id == self.id).first()
        if not result:
            return 0

        return result.count

    def post_count(self):
        result = db.session().query(db.func.count(Post.id).label('count')).join(Thread, db.and_(Thread.id == Post.thread_id)).filter(Thread.board_id == self.id).first()
        if not result:
            return 0

        return result.count

class BoardSchema(marsh.ModelSchema):
    last_thread_age = marsh.Method('_last_thread_age')
    last_post_age = marsh.Method('_last_post_age')
    thread_count = marsh.Method('_thread_count')
    post_count = marsh.Method('_post_count')

    class Meta:
        model = Board

    def _thread_count(self, board):
        return board.thread_count()

    def _post_count(self, board):
        return board.post_count()

    def _last_thread_age(self, board):
        return int(time()) - board.last_thread

    def _last_post_age(self, board):
        return int(time()) - board.last_post

board_schema = BoardSchema()
