from hashlib import sha1
from time import time

from confab import db, marsh
from .post import Post, PostSchema

def thread_get_hash(context):
    params = context.get_current_parameters()
    if not params:
        params = {}

    board_id = params.get('board_id', 0)

    h = sha1()
    h.update('.'.join(map(str, [time(), board_id, time()])).encode())
    return h.hexdigest()[:8]

class Thread(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    board_id = db.Column(db.Integer, db.ForeignKey('board.id', ondelete='CASCADE'))
    hash = db.Column(db.String(8), default=thread_get_hash)
    title = db.Column(db.String(128))
    link = db.Column(db.Text, nullable=True)
    domain = db.Column(db.Text, nullable=True)
    content = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.Integer, default=lambda: int(time()))
    bumped_at = db.Column(db.Integer, default=lambda: int(time()))
    nsfw = db.Column(db.Boolean, default=False)
    locked = db.Column(db.Boolean, default=False)
    sticky = db.Column(db.Boolean, default=False)
    score = db.Column(db.Integer, default=1)

    posts = db.relationship('Post', backref='thread')

    def post_count(self):
        result = db.session().query(db.func.count(Post.id).label('count')).filter(Post.thread_id == self.id).first()
        if not result:
            return 0

        return result.count

class ThreadSchema(marsh.ModelSchema):
    exclude = ('score')

    age = marsh.Method('_age')
    visible_score = marsh.Method('_visible_score')

    posts = marsh.Nested(PostSchema, many=True)

    class Meta:
        model = Thread

    def _age(self, thread):
        return int(time()) - thread.created_at

    def _visible_score(self, thread):
        if self._age(thread) < (2 * 60 * 60):
            return 0

        return thread.score

thread_schema = ThreadSchema()
