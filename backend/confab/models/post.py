from hashlib import sha1
from time import time

from confab import db, marsh

def post_get_hash(context):
    params = context.get_current_parameters()
    if not params:
        params = {}

    thread_id = params.get('thread_id', 0)

    h = sha1()
    h.update('.'.join(map(str, [time(), thread_id, time()])).encode())
    return h.hexdigest()[:8]

class Post(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    thread_id = db.Column(db.Integer, db.ForeignKey('thread.id', ondelete='CASCADE'))
    hash = db.Column(db.String(8), default=post_get_hash)
    content = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.Integer, default=lambda: int(time()))

class PostSchema(marsh.ModelSchema):
    age = marsh.Method('_age')

    class Meta:
        model = Post

    def _age(self, post):
        return int(time()) - post.created_at

post_schema = PostSchema()
