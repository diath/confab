from time import time

from flask import jsonify, request

from confab import app, db
from confab import Board, Thread, ThreadForm
from confab import thread_schema
from confab import message_response, error_response, extract_domain

@app.route('/api/v1/thread', methods=['POST'])
def api_thread_post():
    form = ThreadForm()
    if not form.validate():
        return error_response(form.errors)

    board = Board.query.filter(Board.id == form.board_id.data).first()
    if not board:
        return error_response('The target board does not exist.')

    thread = Thread()
    thread.board_id = board.id
    thread.title = form.title.data
    thread.link = form.link.data
    thread.content = form.content.data

    if thread.link:
        domain = extract_domain(thread.link)
        if domain:
            thread.domain = domain

    if board.nsfw:
        thread.nsfw = form.nsfw.data

    if board.thread_count() >= board.thread_limit:
        # TODO: This should probably remove all threads exceeding the limit in case the limit is shrunk.
        last = Thread.query.order_by(Thread.bumped_at.asc()).first()
        if last:
            db.session().delete(last)

    board.last_thread = int(time())

    db.session().add(thread)
    db.session().commit()

    return jsonify(thread_schema.dump(thread))

@app.route('/api/v1/thread', methods=['GET'])
def api_threads_get():
    board_id = request.args.get('board_id', type=int, default=0)
    board = Board.query.filter(Board.id == board_id).first()
    if not board:
        return error_response('The target board does not exist.')

    threads = Thread.query.filter(Thread.board_id == board_id).order_by(
        Thread.sticky.desc(), Thread.bumped_at.desc()
    ).all()
    return jsonify(thread_schema.dump(threads, many=True))

@app.route('/api/v1/thread/<int:id>', methods=['GET'])
def api_thread_get(id):
    thread = Thread.query.filter(Thread.id == id).first()
    return jsonify(thread_schema.dump(thread))

@app.route('/api/v1/thread/<string:hash>', methods=['GET'])
def api_thread_get_hash(hash):
    thread = Thread.query.filter(Thread.hash == hash).first()
    return jsonify(thread_schema.dump(thread))

@app.route('/api/v1/thread/<int:id>', methods=['PUT'])
def api_thread_put(id):
    thread = Thread.query.filter(Thread.id == id).first()
    if not thread:
        return error_response('The thread does not exist.')

    return message_response('The thread has been updated.')

@app.route('/api/v1/thread/<int:id>', methods=['DELETE'])
def api_thread_delete(id):
    thread = Thread.query.filter(Thread.id == id).first()
    if not thread:
        return error_response('The thread does not exist.')

    db.session().delete(thread)
    db.session().commit()

    return message_response('The thread has been deleted.')
