from flask import jsonify

from confab import app, db
from confab import Board, BoardForm
from confab import board_schema
from confab import message_response, error_response

@app.route('/api/v1/board', methods=['POST'])
def api_board_post():
    form = BoardForm()
    if not form.validate():
        return error_response(form.errors)

    board = Board.query.filter(Board.uri == form.uri.data).first()
    if board:
        return error_response('A board with that uri already exists.')

    board = Board()
    board.uri = form.uri.data
    board.name = form.name.data
    board.description = form.description.data
    board.nsfw = form.nsfw.data

    db.session().add(board)
    db.session().commit()

    return jsonify(board_schema.dump(board))

@app.route('/api/v1/board', methods=['GET'])
def api_boards_get():
    boards = Board.query.all()
    return jsonify(board_schema.dump(boards, many=True))

@app.route('/api/v1/board/<string:uri>', methods=['GET'])
def api_board_get(uri):
    board = Board.query.filter(Board.uri == uri).first()
    return jsonify(board_schema.dump(board))

@app.route('/api/v1/board/<string:uri>', methods=['PUT'])
def api_board_put(uri):
    board = Board.query.filter(Board.uri == uri).first()
    if not board:
        return error_response('A board with that uri does not exist.')

    return message_response('The board has been updated.')

@app.route('/api/v1/board/<string:uri>', methods=['DELETE'])
def api_board_delete(uri):
    board = Board.query.filter(Board.uri == uri).first()
    if not board:
        return error_response('A board with that uri does not exist.')

    db.session().delete(board)
    db.session().commit()

    return message_response('The board has been deleted.')
