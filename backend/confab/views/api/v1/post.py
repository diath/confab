from time import time

from flask import jsonify, request

from confab import app, db
from confab import Board, Thread, Post, PostForm
from confab import post_schema
from confab import message_response, error_response

@app.route('/api/v1/post', methods=['POST'])
def api_post_post():
    form = PostForm()
    if not form.validate():
        return error_response(form.errors)

    thread = Thread.query.filter(Thread.id == form.thread_id.data).first()
    if not thread:
        return error_response('The target thread does not exist.')

    if thread.locked:
        return error_response('The target thread is locked.')

    board = Board.query.filter(Board.id == thread.board_id).first()
    if not board:
        return error_response('The parent board does not exist.')

    post = Post()
    post.thread_id = form.thread_id.data
    post.content = form.content.data

    if thread.post_count() < board.bump_limit:
        thread.bumped_at = int(time())

    board.last_post = int(time())

    db.session().add(post)
    db.session().commit()

    return jsonify(post_schema.dump(post))

@app.route('/api/v1/post', methods=['GET'])
def api_posts_get():
    thread_id = request.args.get('thread_id', type=int, default=0)
    thread = Thread.query.filter(Thread.id == thread_id).first()
    if not thread:
        return error_response('The target thread does not exist.')

    posts = Post.query.filter(Post.thread_id == thread_id).all()
    return jsonify(post_schema.dump(posts, many=True))

@app.route('/api/v1/post/<int:id>', methods=['GET'])
def api_post_get(id):
    post = Post.query.filter(Post.id == id).first()
    return jsonify(post_schema.dump(post))

@app.route('/api/v1/post/<int:id>', methods=['PUT'])
def api_post_put(id):
    post = Post.query.filter(Post.id == id).first()
    if not post:
        return error_response('The post does not exist.')

    return message_response('The post has been updated.')

@app.route('/api/v1/post/<int:id>', methods=['DELETE'])
def api_post_delete(id):
    post = Post.query.filter(Post.id == id).first()
    if not post:
        return error_response('The post does not exist.')

    db.session().delete(post)
    db.session().commit()

    return message_response('The post has been deleted.')
