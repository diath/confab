from urllib.parse import urlparse

from flask import jsonify

def message_response(msg):
    if isinstance(msg, str):
        msg = [msg]

    return jsonify({"messages": msg})

def error_response(err):
    if isinstance(err, str):
        err = [err]

    return jsonify({"errors": err})

def extract_domain(link):
    try:
        return urlparse(link).netloc
    except ValueError:
        return ''
