if __name__ == '__main__':
    from confab import app, db
    db.create_all()
    app.run()
