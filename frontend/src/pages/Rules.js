import React from 'react'

import Panel from '../components/Panel'

export default class RulesPage extends React.Component {
	componentWillMount() {
		document.title = 'confab.me - Rules & Guidelines'
	}

	render() {
		return <div>
			<Panel title="Rules">
				<ul>
					<li>Content violating the laws of the United States will be deleted and any server logs (if still present) related to the poster may be handed over to any US law enforcement representatives upon request.</li>
					<li>Posting personal information (such as home address or a phone number) of a private person is strictly forbidden.</li>
					<li>NSFW content posted on SFW boards or NSFW content not marked as such will be deleted.</li>
					<li>no blatant advertisement threads</li>
				</ul>
			</Panel>
			<Panel title="Guidelines">
				<ul>
					<li>Do not bump your own threads if they do not gain traction, instead try resubmitting at a different time.</li>
					<li>Resort to namecalling only if you have an actual argument to go with it.</li>
				</ul>
			</Panel>
		</div>
	}
}
