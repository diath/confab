import React from 'react'

import Panel from '../components/Panel'

export default class AboutPage extends React.Component {
	componentWillMount() {
		document.title = 'confab.me - About'
	}

	render() {
		return <Panel title="About">
			<p>
				Thread score displays only for threads that have been up for 2 hours to avoid bump bias.
			</p>
		</Panel>
	}
}
