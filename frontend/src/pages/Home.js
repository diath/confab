import React from 'react'

import Boards from '../components/Boards'

function Home() {
	return <div>
		<Boards />
	</div>
}

export default Home
