export const SET_BOARDS = 'SET_BOARDS'
export const SET_BOARD = 'SET_BOARD'
export const SET_THREADS = 'SET_THREADS'
export const SET_THREAD = 'SET_THREAD'
export const SET_FORM_ERRORS = 'SET_FORM_ERRORS'
export const REMOVE_FORM_ERROR = 'REMOVE_FORM_ERROR'

export function setBoards(boards) {
	return { type: SET_BOARDS, boards }
}

export function setBoard(board) {
	return { type: SET_BOARD, board }
}

export function setThreads(threads) {
	return { type: SET_THREADS, threads }
}

export function setThread(thread) {
	return { type: SET_THREAD, thread }
}

export function setFormErrors(errors) {
	return { type: SET_FORM_ERRORS, errors }
}

export function removeFormError(error) {
	return { type: REMOVE_FORM_ERROR, error }
}
