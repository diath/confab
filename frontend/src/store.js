import { createStore } from 'redux'
import * as actions from './actions'

function reducer(state = {}, action) {
	switch (action.type) {
		case actions.SET_BOARDS:
			return { ...state, boards: action.boards }
		case actions.SET_BOARD:
			return { ...state, board: action.board }
		case actions.SET_THREADS:
			return { ...state, threads: action.threads }
		case actions.SET_THREAD:
			return { ...state, thread: action.thread }
		case actions.SET_FORM_ERRORS:
			let errors = []
			if (Array.isArray(action.errors)) {
				errors = action.errors
			} else {
				for (let key of Object.keys(action.errors)) {
					action.errors[key].map((message) => errors.push(`${key}: ${message}`))
				}
			}

			return { ...state, formErrors: errors }
		case actions.REMOVE_FORM_ERROR:
			let removeFormErrors = state.formErrors || []
			let index = removeFormErrors.indexOf(action.error)
			if (index != -1) {
				removeFormErrors.splice(index, 1)
			}

			return { ...state, formErrors: removeFormErrors }
		default:
			return state
	}
}

const store = createStore(reducer)
export default store
