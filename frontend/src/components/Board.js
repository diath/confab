import React from 'react'
import { Link } from 'react-router-dom'

import './Board.css'
import { age } from '../util'

export default class Board extends React.Component {
	constructor(props) {
		super(props)
		this.props = props
	}

	render() {
		let board = this.props.board

		let sfw = 'SFW'
		if (board.nsfw) {
			sfw = 'NSFW'
		}

		return <div className="board">
			<div className="board-title">
				<Link to={`/b/${board.uri}`}>
					/{board.uri}/ - {board.name} - {board.description}
				</Link>
			</div>
			<div className="board-footer">
				{sfw} | Threads: {board.thread_count} | Posts: {board.post_count} | Thread Limit: {board.thread_limit} | Bump Limit: {board.bump_limit} | Last Thread: {age(board.last_thread_age)} | Last Post: {age(board.last_post_age)}
			</div>
		</div>
	}
}
