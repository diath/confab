import React from 'react'
import {
	BrowserRouter as Router, Switch, Route
} from 'react-router-dom'
import {
	Provider
} from 'react-redux'

import './App.css'

import store from '../store'

import Header from './Header'
import FormErrors from './FormErrors'

import HomePage from '../pages/Home'
import RulesPage from '../pages/Rules'
import AboutPage from '../pages/About'

import ThreadList from '../components/ThreadList'
import Thread from '../components/Thread'

function App() {
	return (
		<Router>
			<Provider store={store}>
				<div>
					<Header />
					<FormErrors />

					<Switch>
						<Route exact path="/" component={HomePage} />
						<Route path="/rules" component={RulesPage} />
						<Route path="/about" component={AboutPage} />
						<Route path="/b/:uri/:hash" component={Thread} />
						<Route path="/b/:uri" component={ThreadList} />
					</Switch>
				</div>
			</Provider>
		</Router>
	)
}

export default App;
