import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import {
	Button, Classes, Checkbox,
	Dialog, FormGroup, InputGroup,
	TextArea
} from '@blueprintjs/core'

import './ThreadList.css'

import ThreadEntry from './ThreadEntry'
import { requestBoard, requestThreadList, submitThread } from '../api'

class ThreadList extends React.Component {
	constructor(props, context) {
		super(props, context)
		this.props = { ...props, threads: [] }
		this.state = { showDialog: false, threadTitle: '', threadLink: '', threadContent: '', threadNsfw: false, threadAnonymous: false }
	}

	componentDidMount() {
		requestBoard(this.props.match.params.uri, (board) => {
			document.title = `/${board.uri}/ - ${board.name}`
			requestThreadList(board)
		})
	}

	toggleDialog = () => {
		this.setState({ showDialog: !this.state.showDialog })
	}

	cancelForm = () => {
		this.setState({
			showDialog: false,
			threadTitle: '',
			threadLink: '',
			threadContent: '',
			threadNsfw: false,
			threadAnonymous: false
		})
	}

	submitForm = () => {
		submitThread(this.props.board, this.state)
		this.cancelForm()
	}

	// TODO: Coalse these handlers?
	onTitleChange = (event) => {
		this.setState({ threadTitle: event.target.value })
	}

	onLinkChange = (event) => {
		this.setState({ threadLink: event.target.value })
	}

	onContentChange = (event) => {
		this.setState({ threadContent: event.target.value })
	}

	onNsfwChange = (event) => {
		this.setState({ threadNsfw: event.target.checked })
	}

	onAnonymousChange = (event) => {
		this.setState({ threadAnonymous: event.target.checked })
	}

	render() {
		return <div id="threads">
			<Dialog autoFocus={true} isOpen={this.state.showDialog} title="Submit Thread" onClose={this.cancelForm}>
				<div className={Classes.DIALOG_BODY}>
					<FormGroup
						label="Thread Title"
						labelFor="i-thread-title"
						labelInfo="(required)"
					>
						<InputGroup id="i-thread-title" placeholder="Thread Title" onChange={this.onTitleChange} value={this.state.threadTitle} />
					</FormGroup>
					<FormGroup
						label="Thread Link"
						labelFor="i-thread-link"
						labelInfo="(optional)"
					>
						<InputGroup id="i-thread-link" placeholder="Thread Link" onChange={this.onLinkChange} value={this.state.threadLink} />
					</FormGroup>
					<FormGroup
						label="Content"
						labelFor="i-thread-content"
						labelInfo="(optional with link; otherwise required)"
						helperText="Markdown supported."
					>
						<TextArea id="i-thread-content" placeholder="Content" fill="true" onChange={this.onContentChange} value={this.state.threadContent} />
					</FormGroup>
					<FormGroup label="Options">
						{this.props.board.nsfw && <Checkbox label="NSFW" onChange={this.onNsfwChange} checked={this.state.threadNsfw} />}
						<Checkbox label="Anonymous" onChange={this.onAnonymousChange} checked={this.state.threadAnonymous} />
					</FormGroup>
				</div>
				<div className={Classes.DIALOG_FOOTER}>
					<div className={Classes.DIALOG_FOOTER_ACTIONS}>
						<Button onClick={this.cancelForm}>Cancel</Button>
						<Button onClick={this.submitForm}>Submit</Button>
					</div>
				</div>
			</Dialog>
			<div id="board-title">
				<Link to={`/b/${this.props.board.uri}`}>/{this.props.board.uri}/ - {this.props.board.name}</Link>
			</div>
			<div id="board-description">
				{this.props.board.description}
				<br />
				<Button onClick={this.toggleDialog}>Submit Thread</Button>
			</div>
			{this.props.threads.map(thread => <ThreadEntry key={thread.uri} board={this.props.board} thread={thread} />)}
		</div>
	}
}

function mapStateToProps(state) {
	return {
		board: state.board || {},
		threads: state.threads || [],
		showDialog: false,
	}
}

export default connect(mapStateToProps)(ThreadList)
