import React from 'react'
import { Link } from 'react-router-dom'

import './Header.css'

function Header() {
	return <div id="header">
		<div id="header-container">
			<div id="header-left">
				<ul className="navigation">
					<li><Link to="/">Home</Link></li>
					<li><a href="/boards">Boards</a></li>
					<li><Link to="/rules">Rules &amp; Guidelines</Link></li>
					<li><Link to="/about">About</Link></li>
				</ul>
			</div>
			<div id="header-right">
				<ul className="navigation navigation-right">
					<li><a href="/login">Login</a></li>
					<li><a href="/register">Register</a></li>
					<li><a href="#">...</a></li>
				</ul>
			</div>
		</div>
	</div>
}

export default Header
