import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import {
	Button, Classes, Checkbox,
	Dialog, FormGroup, InputGroup,
	TextArea
} from '@blueprintjs/core'

import './Thread.css'
import { requestBoard, requestThread, submitPost } from '../api'
import Post from './Post'
import { age } from '../util'

class Thread extends React.Component {
	constructor(props) {
		super(props)
		this.state = { showDialog: false, board: null, thread: null, postContent: '', postAnonymous: false }
	}

	componentDidMount() {
		requestBoard(this.props.match.params.uri, (board) => {
			this.setState({ board: board })
			this.setTitle()
		})

		requestThread(this.props.match.params.hash, (thread) => {
			this.setState({ thread: thread })
			this.setTitle()
		})
	}

	setTitle() {
		let board = this.state.board
		let thread = this.state.thread

		if (board && !thread) {
			document.title = `/${board.uri}/ - Loading...`
		} else if (board && thread) {
			document.title = `/${board.uri}/ - ${thread.title}`
		} else {
			document.title = 'confab.me'
		}
	}

	toggleReplyDialog = () => {
		this.setState({ showDialog: !this.state.showDialog })
	}

	cancelForm = () => {
		this.setState({
			showDialog: false,
			postContent: '',
			postAnonymous: false,
		})
	}

	submitForm = () => {
		submitPost(this.state.thread, this.state)
		this.cancelForm()
	}

	onContentChange = (event) => {
		this.setState({ postContent: event.target.value })
	}

	onAnonymousChange = (event) => {
		this.setState({ postAnonymous: event.target.checked })
	}

	onReply = (post) => {
		this.setState({
			showDialog: true,
			postContent: `>>${post.hash}\n`
		})
	}

	onQuote = (post) => {
		this.setState({
			showDialog: true,
			postContent: `>>${post.hash}\n>${post.content}\n`
		})
	}

	render() {
		let thread = this.state.thread
		let board = this.state.board

		if (!board || !thread) {
			return <div className="thread">
				<div className="thread-title">
					/{board ? board.uri : 'Unknown'}/
				</div>
				<div className="thread-footer">
					Loading...
				</div>
			</div>
		}

		let locked = ''
		if (thread.locked) {
			locked = '[Locked] '
		}

		let sticky = ''
		if (thread.sticky) {
			sticky = '[Sticky] '
		}

		let link = ''
		if (thread.link && thread.link.length > 0) {
			link = <a href={thread.link} className="thread-link">{thread.title}</a>
		} else {
			link = <Link to={`/b/${board.uri}/${thread.hash}`} className="thread-link">{thread.title}</Link>
		}

		let domain = ' (self)'
		if (thread.domain && thread.domain.length > 0) {
			domain = ` (${thread.domain})`
		}

		let score = 'Score: Hidden'
		if (thread.visible_score > 0) {
			score = `Score: ${thread.visible_score}`
		}

		return <div>
			<Dialog autoFocus={true} isOpen={this.state.showDialog} title="Post Reply" onClose={this.cancelForm}>
				<div className={Classes.DIALOG_BODY}>
					<FormGroup
						label="Content"
						labelFor="i-post-content"
						helperText="Markdown supported."
					>
						<TextArea id="i-post-content" placeholder="Content" fill="true" onChange={this.onContentChange} value={this.state.postContent} />
					</FormGroup>
					<FormGroup label="Options">
						<Checkbox label="Anonymous" onChange={this.onAnonymousChange} checked={this.state.postAnonymous} />
					</FormGroup>
				</div>
				<div className={Classes.DIALOG_FOOTER}>
					<div className={Classes.DIALOG_FOOTER_ACTIONS}>
						<Button onClick={this.cancelForm}>Cancel</Button>
						<Button onClick={this.submitForm}>Submit</Button>
					</div>
				</div>
			</Dialog>
			<div id="board-title">
				<Link to={`/b/${board.uri}`}>/{board.uri}/ - {board.name}</Link>
			</div>
			<div id="board-description">
				{board.description}
				<br />
				<Button onClick={this.toggleReplyDialog}>Post Reply</Button>
			</div>
			<div className="thread">
				<div className="thread-title">
					{link} <span className="thread-domain">{domain}</span>
				</div>
				<div className="thread-footer">
					{locked}{sticky}Age: {age(thread.age)} | {score} <a href="#" title="Upvote">[+]</a> | Submitter: <a href="#" title="Submitter">Anonymous</a>
				</div>
				<div className="thread-content">
					{thread.content}
				</div>
			</div>
			{thread.posts.map(post => <Post key={post.hash} post={post} onReply={this.onReply} onQuote={this.onQuote} />)}
		</div>
	}
}

export default Thread
