import React from 'react'
import { connect } from 'react-redux'

import './Boards.css'
import Board from './Board'
import { requestBoardList } from '../api'

class Boards extends React.Component {
	componentWillMount() {
		document.title = 'confab.me'
	}

	componentDidMount() {
		requestBoardList()
	}

	render() {
		return <div id="boards">
			{this.props.boards.map(board => <Board key={board.uri} board={board} />)}
		</div>
	}
}

function mapStateToProps(state) {
	return {
		boards: state.boards || []
	}
}

export default connect(mapStateToProps)(Boards)
