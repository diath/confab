import React from 'react'
import { connect } from 'react-redux'

import { Intent, Toast, Toaster } from '@blueprintjs/core'

import './FormErrors.css'

import store from '../store'
import { removeFormError } from '../actions'

class FormErrors extends React.Component {
	constructor(props, context) {
		super(props, context)
		this.props = { ...props, formErrors: [] }
	}

	onDismiss = (message) => {
		store.dispatch(removeFormError(message))
	}

	render() {
		return <div id="form-errors">
			<Toaster>
				{this.props.formErrors.map((error) => <Toast intent={Intent.DANGER} message={error} onDismiss={() => this.onDismiss(error)} />)}
			</Toaster>
		</div>
	}
}

function mapStateToProps(state) {
	return {
		formErrors: state.formErrors || [],
	}
}

export default connect(mapStateToProps)(FormErrors)
