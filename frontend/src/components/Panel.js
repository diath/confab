import React from 'react'

import './Panel.css'

export default class Panel extends React.Component {
	render() {
		return (
			<div className="panel">
				<div className="panel-title">
					{this.props.title}
				</div>
				<div className="panel-content">
					{this.props.children}
				</div>
			</div>
		);
	}
}
