import React from 'react'

import {
	Button, ButtonGroup
} from '@blueprintjs/core'

import './Post.css'
import PostRenderer from './PostRenderer'
import { age } from '../util'

export default class Post extends React.Component {
	constructor(props) {
		super(props)
		this.props = props
	}

	onReply = () => {
		this.props.onReply(this.props.post)
	}

	onQuote = () => {
		this.props.onQuote(this.props.post)
	}

	render() {
		let post = this.props.post
		return <div id={`p${post.hash}`} className="post">
			<div className="post-title">
				<div className="post-title-left">
					Anonymous | Age: {age(post.age)} | ID: <a href={`#p${post.hash}`}>{post.hash}</a>
				</div>
				<div className="post-title-right">
					<ButtonGroup>
						<Button onClick={this.onReply}>Reply</Button>
						<Button onClick={this.onQuote}>Quote</Button>
					</ButtonGroup>
				</div>
			</div>
			<div className="post-content">
				<PostRenderer source={post.content} />
			</div>
		</div>
	}
}
