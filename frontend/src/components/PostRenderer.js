import React from 'react'
import ReactMarkdown from 'react-markdown'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { tomorrowNightBright } from 'react-syntax-highlighter/dist/esm/styles/hljs'

const DisallowedTypes = [
	'blockquote', 'image', 'imageReference', 'definition',
]

class CodeBlockRenderer extends React.PureComponent {
	render() {
		const { language, value } = this.props
		return <SyntaxHighlighter language={language} style={tomorrowNightBright}>
			{value}
		</SyntaxHighlighter>
	}
}

export default class PostRenderer extends React.Component {
	render() {
		return <ReactMarkdown
			source={this.props.source.replace('\n', '\n\n')}
			skipHtml={false}
			escapeHtml={true}
			disallowedTypes={DisallowedTypes}
			unwrapDisallowed={true}
			renderers={{
				code: CodeBlockRenderer,
				inlineCode: CodeBlockRenderer,
			}}
		/>
	}
}
