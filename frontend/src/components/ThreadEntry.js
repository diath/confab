import React from 'react'
import { Link } from 'react-router-dom'

import './ThreadEntry.css'
import { age } from '../util'

export default class ThreadEntry extends React.Component {
	constructor(props) {
		super(props)
		this.props = props
	}

	render() {
		let thread = this.props.thread

		let locked = ''
		if (thread.locked) {
			locked = '[Locked] '
		}

		let sticky = ''
		if (thread.sticky) {
			sticky = '[Sticky] '
		}

		let link = ''
		if (thread.link && thread.link.length > 0) {
			link = <a href={thread.link} className="thread-link">{thread.title}</a>
		} else {
			link = <Link to={`/b/${this.props.board.uri}/${thread.hash}`} className="thread-link">{thread.title}</Link>
		}

		let domain = ' (self)'
		if (thread.domain && thread.domain.length > 0) {
			domain = ` (${thread.domain})`
		}

		let score = 'Score: Hidden'
		if (thread.visible_score > 0) {
			score = `Score: ${thread.visible_score}`
		}

		return <div className="thread">
			<div className="thread-title">
				{link} <span className="thread-domain">{domain}</span>
			</div>
			<div className="thread-footer">
				{locked}{sticky}Age: {age(thread.age)} | {score} <a href="#" title="Upvote">[+]</a> | Submitter: <a href="#" title="Submitter">Anonymous</a> | <Link to={`/b/${this.props.board.uri}/${thread.hash}`}>{thread.posts.length} posts</Link>
			</div>
		</div>
	}
}
