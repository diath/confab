import axios from 'axios'

import store from './store'
import { setBoards, setBoard, setThreads, setThread, setFormErrors } from './actions'

const api = axios.create({
	baseURL: 'http://127.0.0.1:5000/api/v1',
	headers: {
		'Accept': 'application/json'
	}
})

export function requestBoardList() {
	api
		.get('/board')
		.then((response) => {
			store.dispatch(setBoards(response.data))
		})
	// TODO: catch dispatch errors
}

export function requestBoard(uri, callback) {
	api
		.get(`/board/${uri}`)
		.then((response) => {
			store.dispatch(setBoard(response.data))

			if (callback) {
				callback(response.data)
			}
		})
	// TODO: catch dispatch errors
}

export function requestThreadList(board) {
	api
		.get(`/thread?board_id=${board.id}`)
		.then((response) => {
			store.dispatch(setThreads(response.data))
		})
	// TODO: catch dispatch errors
}

export function requestThread(hash, callback) {
	api
		.get(`/thread/${hash}`)
		.then((response) => {
			store.dispatch(setThread(response.data))

			if (callback) {
				callback(response.data)
			}
		})
}

export function submitThread(board, data) {
	api
		.post('/thread', {
			board_id: board.id,
			title: data.threadTitle,
			link: data.threadLink,
			content: data.threadLink,
			nsfw: data.threadNsfw,
			anonymous: data.threadanonymous,
		})
		.then((response) => {
			if (response.data.errors) {
				store.dispatch(setFormErrors(response.data.errors))
			} else {
			}
		})
}

export function submitPost(thread, data) {
	api
		.post('/post', {
			thread_id: thread.id,
			content: data.postContent,
			anonymous: data.postAnonymous,
		}).then((response) => {
			console.log(response.data)
			if (response.data.errors) {
				store.dispatch(setFormErrors(response.data.errors))
			} else {
			}
		})
}

export default api
