export function age(time) {
	if (time < 60) {
		return `${time}s`
	} else if (time < 60 * 60) {
		return `${Math.ceil(time / 60)}m`
	}

	return `${Math.ceil(time / 60 / 60)}h`
}

const util = {}
util.age = age

export default util
